
// enum type CompanyType
export let CompanyType = {
	S_Corp:"S_Corp",
	LLC:"LLC",
	C_Corp:"C_Corp",
}

// enum type EmploymentType
export let EmploymentType = {
	Manager:"Manager",
	Board_Member:"Board_Member",
	Team_Lead:"Team_Lead",
	Consultant:"Consultant",
	Vice_President:"Vice_President",
	Sr_Mananager:"Sr_Mananager",
	Director:"Director",
	Engineer:"Engineer",
}

// enum type Industry
export let Industry = {
	Bank:"Bank",
	Insurance:"Insurance",
	Manufacturer:"Manufacturer",
	Technology:"Technology",
	Health:"Health",
	Financial:"Financial",
}
