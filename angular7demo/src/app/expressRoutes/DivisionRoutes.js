// divisionRoutes.js

var express = require('express');
var app = express();
var divisionRoutes = express.Router();

// Require Item model in our routes module
var Division = require('../models/Division');

// Defined store route
divisionRoutes.route('/add').post(function (req, res) {
	var division = new Division(req.body);
	division.save()
    .then(item => {
    	res.status(200).json({'division': 'Division added successfully'});
    })
    .catch(err => {
    	res.status(400).send("unable to save to database");
    });
});

// Defined get data(index or listing) route
divisionRoutes.route('/').get(function (req, res) {
	Division.find(function (err, divisions){
		if(err){
			console.log(err);
		}
		else {
			res.json(divisions);
		}
	});
});

// Defined edit route
divisionRoutes.route('/edit/:id').get(function (req, res) {
	var id = req.params.id;
	Division.findById(id, function (err, division){
		res.json(division);
	});
});

//  Defined update route
divisionRoutes.route('/update/:id').post(function (req, res) {
	Division.findById(req.params.id, function(err, division) {
		if (!division)
			return next(new Error('Could not load a Division Document using id ' + req.params.id));
		else {
            division.name = req.body.name;
            division.Head = req.body.Head;

			division.save().then(division => {
				res.json('Update complete');
			})
			.catch(err => {
				res.status(400).send("unable to update the database");
			});
		}
	});
});

// Defined delete | remove | destroy route
divisionRoutes.route('/delete/:id').get(function (req, res) {
   Division.findOneAndDelete({_id: req.params.id}, function(err, division){
        if(err) res.json(err);
        else res.json('Successfully removed ' + Division + ' using id ' + req.params.id );
    });
});

module.exports = divisionRoutes;