import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { DivisionService } from '../../../services/Division.service';
import { FormGroup,  FormBuilder,  Validators } from '@angular/forms';
import { SubBaseComponent } from '../../Division/sub.base.component';

@Component({
  selector: 'app-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.css']
})
export class EditDivisionComponent extends SubBaseComponent implements OnInit {

  division: any;
  divisionForm: FormGroup;
  title = 'Edit Division';
  
  constructor( http: HttpClient, 
  				private route: ActivatedRoute, 
  				private router: Router, 
  				private service: DivisionService, 
  				private fb: FormBuilder) {
    super(http);
    this.createForm();
   }

  createForm() {
    this.divisionForm = this.fb.group({
      name: ['', Validators.required],
      Head: ['', ]
   });
  }
  updateDivision(name, Head) {
    this.route.params.subscribe(params => {
    	this.service.updateDivision(name, Head, params['id'])
      		.then(success => this.router.navigate(['/indexDivision']) );
  });
}

// initialization
  ngOnInit() {
    this.route.params.subscribe(params => {
      this.division = this.service.editDivision(params['id']).subscribe(res => {
        this.division = res;
      });
    });
    
    super.ngOnInit();
  }
}
