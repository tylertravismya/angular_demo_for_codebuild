import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { AddressService } from '../../../services/Address.service';
import { FormGroup,  FormBuilder,  Validators } from '@angular/forms';
import { BaseComponent } from '../../base.component';
import { SubBaseComponent } from '../../Address/sub.base.component';

@Component({
  selector: 'app-create',
  templateUrl: './create.component.html',
  styleUrls: ['./create.component.css']
})
export class CreateAddressComponent extends SubBaseComponent implements OnInit {

  title = 'Add Address';
  addressForm: FormGroup;
  
  constructor(  http: HttpClient,
  				private addressservice: AddressService, 
  				private fb: FormBuilder, 
  				private router: Router) {
	super(http);
    this.createForm();
   }
  createForm() {
    this.addressForm = this.fb.group({
      street: ['', Validators.required],
      city: ['', Validators.required],
      state: ['', Validators.required],
      zipCode: ['', Validators.required]
   });
  }
  addAddress(street, city, state, zipCode) {
      this.addressservice.addAddress(street, city, state, zipCode)
      	.then(success => this.router.navigate(['/indexAddress']) );
  }
  
// initialization  
  ngOnInit() {
  	super.ngOnInit();
  }
}
